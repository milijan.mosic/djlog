from django.shortcuts import render
from .models import *


def index(response):
  articles = Article.objects.filter(published=True)
  return render(response, "index.html", {"articles": articles})

def article(response, id):
  article = Article.objects.get(id=id)
  return render(response, "article.html", {"article": article})
