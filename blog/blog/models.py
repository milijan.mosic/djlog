from django.db import models
from django.core import serializers


class Article(models.Model):
    id = models.AutoField(primary_key=True)
    published = models.BooleanField(default=False)
    title = models.CharField(max_length=255)
    thumbnail = models.ImageField(upload_to='thumbnails/articles/')
    description = models.TextField(max_length=1024)
    body = models.TextField(max_length=65535)
    date_created = models.DateField(auto_now=True)
    date_modified = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title

    class Meta:
        ordering = ['date_created']
