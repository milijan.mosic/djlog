#/bin/bash

docker-compose exec django python manage.py makemigrations
docker-compose exec django python manage.py migrate
docker-compose exec django python manage.py createsuperuser

# Make .env file where django settings.py file is
# Make .env file where next package.json file is
